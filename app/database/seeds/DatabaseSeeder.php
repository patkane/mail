<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        //$this->call('UserTableSeeder');
        $this->call('EmailsTableSeeder');
        $this->call('SubscriptionsTableSeeder');
        $this->call('ContactsSubscriptionsTableSeeder');
        $this->call('ContactsTableSeeder');
        $this->call('MailingListsTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('OperatorNamesTableSeeder');

	}
}
