<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Mail\ContactsSubscriptions;

class ContactsSubscriptionsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 75) as $index) {
            ContactsSubscriptions::create([
                'contact_id' => $faker->randomNumber(1, 30),
                'subscription_id' => $faker->randomNumber(1, 5)
            ]);
        }
    }
}
