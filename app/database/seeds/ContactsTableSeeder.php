<?php

use Faker\Factory as Faker;
use Mail\Contact;

class ContactsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        // DB::table('contacts')->truncate();

        $faker = Faker::create();

        foreach (range(1, 30) as $index) {
            Contact::create([
                'user_id' => $faker->randomNumber(1, 5),
                'owner_id' => $faker->randomNumber(1, 10),
                'email' => $faker->email,
                'password' => $faker->md5,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'birthdate' => $faker->date($format = 'Y-m-d'),
                'phone' => $faker->phoneNumber,
                'address' => $faker->streetAddress,
                'secondary_address' => $faker->secondaryAddress,
                'city' => $faker->city,
                'state' => $faker->stateAbbr,
                'zip' => $faker->postCode,
                'country' => $faker->country,
                'gender' => $faker->randomElement($array = array('M', 'F')),
                'shirt_size' => $faker->randomElement($array = array('XS', 'S', 'M', 'L', 'XL')),
                'confirmation_status' => 1

            ]);
        }
    }

}
