<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Mail\Subscription;

class SubscriptionsTableSeeder extends Seeder
{

    public function run()
    {
        //$faker = Faker::create();

        
        Subscription::create([
            'user_id' => 1,
            'name' => 'Events I am registered for',
            'type' => 'public',
            'status' => 1
        ]);

        Subscription::create([
            'user_id' => 1,
            'name' => 'Events in my area',
            'type' => 'public',
            'status' => 1
        ]);

        Subscription::create([
            'user_id' => 1,
            'name' => 'Newsletters',
            'type' => 'public',
            'status' => 1
        ]);

        Subscription::create([
            'user_id' => 1,
            'name' => 'Upcoming Events',
            'type' => 'public',
            'status' => 1
        ]);
        
        Subscription::create([
            'user_id' => 1,
            'name' => 'Discounts and Promotions',
            'type' => 'public',
            'status' => 1
        ]);
        
    }
}
