<?php

use Faker\Factory as Faker;
use Mail\MailingList;

class MailingListsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        // DB::table('contacts')->truncate();

        $faker = Faker::create();

        foreach (range(1, 20) as $index) {
            MailingList::create([
                'user_id' => $faker->randomNumber(1, 5),
                'owner_id' => $faker->randomNumber(1, 10),
                'name' => $faker->sentence($nbWords = 4),
            ]);
        }
    }

}
