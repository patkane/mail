<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Mail\Role;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        Role::create([
            'name' => 'Super Admin'
            ]);

        Role::create([
            'name' => 'Admin'
            ]);

        Role::create([
            'name' => 'Promotor'
            ]);

        Role::create([
            'name' => 'Basic User'
            ]);
    }
}
