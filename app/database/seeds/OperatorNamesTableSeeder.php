<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Mail\OperatorName;

class OperatorNamesTableSeeder extends Seeder
{
    public function run()
    {
        OperatorName::create([
            'name' => 'Age',
            ]);

        OperatorName::create([
            'name' => 'Gender'
            ]);

        OperatorName::create([
            'name' => 'Location'
            ]);

        OperatorName::create([
            'name' => 'Registered for'
            ]);

        OperatorName::create([
            'name' => 'Shirt Size'
            ]);

        OperatorName::create([
            'name' => 'Confirmation Status'
            ]);


    }
}
