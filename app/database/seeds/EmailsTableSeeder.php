<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Mail\Email;

class EmailsTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 10) as $index) {
            Email::create([
                'user_id' => $faker->randomNumber(1, 10),
                'name' => $faker->sentence($nbWords = 2),
                'subject' => $faker->sentence($nbWords = 6),
                'from_name' => $faker->name,
                'from_email' => $faker->email,
                'reply_to_email' => $faker->email,
                'body' => $faker->text($maxNbChars = 200),
                'status' => $faker->randomElement($array = array ('Draft','Ready for Review','Active')),
                'send_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+21 days'),
                'send_time' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+21 days')
            ]);
        }
    }
}
