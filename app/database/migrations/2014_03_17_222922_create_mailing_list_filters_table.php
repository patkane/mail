<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMailingListFiltersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailing_list_filters', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('mailing_list_id');
            //$table->string('sql_operator');
			$table->string('column_name');
			$table->string('operator');
			$table->string('value');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mailing_list_filters');
    }

}
