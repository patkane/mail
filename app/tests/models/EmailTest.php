<?php

class EmailTest extends TestCase
{

    /**
     * Test to ensure that the entity implements
     * the required interfaces
     *
     * @return void
     */
    public function testInterfacesArePresent()
    {
        $refl = new \ReflectionClass("Email");

        $this->assertTrue(
            $refl->implementsInterface('Contracts\Instances\InstanceInterface'),
            'Email does not implement InstanceInterface'
        );
    }
}
