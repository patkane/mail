<?php

use Services\RoleCreator;
use Contracts\Repositories\RoleRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class RolesController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * Role Repository
     *
     * @var RoleRepositoryInterface
     */
    protected $role;

    public function __construct(RoleRepositoryInterface $role)
    {
        $this->role = $role;
    }

    /**
     * Display a listing of the roles.
     *
     * @return Response
     */
    public function index()
    {
        $roles = $this->role->all();

        return View::make('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new role.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('roles.create');
    }

    /**
     * Store a newly created role in storage.
     *
     * @return Response
     */
    public function store()
    {
        $role_creator = App::make('Services\Roles\RoleCreator');

        return $role_creator->create($this->role, $this, Input::except('_token'));
    }

    /**
     * Handle successful role creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('roles.index')->with('message', 'Role was successfully created');
    }

    /**
     * Handle an error with role creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('roles.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified role.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $role = $this->role->find($id);

        return View::make('roles.show', compact('role'));
    }

    /**
     * Show the form for editing the specified role.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $role = $this->role->find($id);

        return View::make('roles.edit', compact('roles'));
    }

    /**
     * Update the specified role in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $role_updater = App::make('Services\Roles\RoleUpdater');

        return $role_updater->update($this->role, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful role update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('roles.show', $instance->identity());
    }

    /**
     * Handle an error with role update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('roles.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified role from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $role_destroyer = App::make('Services\Roles\RoleDestroyer');

        return $role_destroyer->destroy($this->role, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful role destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('roles.index')->with('message', 'Role was successfully deleted');
    }

    /**
     * Handle an error with role destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('roles.index')->with('message', 'Oops, couldn\'t delete that role');
    }
}
