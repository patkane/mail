<?php

use Services\UserCreator;
use Contracts\Repositories\UserRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class UsersController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * User Repository
     *
     * @var UserRepositoryInterface
     */
    protected $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the users.
     *
     * @return Response
     */
    public function index()
    {
        $users = $this->user->all();

        return View::make('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new user.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('users.create');
    }

    /**
     * Store a newly created user in storage.
     *
     * @return Response
     */
    public function store()
    {
        $user_creator = App::make('Services\Users\UserCreator');

        return $user_creator->create($this->user, $this, Input::except('_token'));
    }

    /**
     * Handle successful user creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('users.index')->with('message', 'User was successfully created');
    }

    /**
     * Handle an error with user creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('users.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified user.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->user->find($id);

        return View::make('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->user->find($id);

        return View::make('users.edit', compact('users'));
    }

    /**
     * Update the specified user in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $user_updater = App::make('Services\Users\UserUpdater');

        return $user_updater->update($this->user, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful user update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('users.show', $instance->identity());
    }

    /**
     * Handle an error with user update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('users.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $user_destroyer = App::make('Services\Users\UserDestroyer');

        return $user_destroyer->destroy($this->user, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful user destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('users.index')->with('message', 'User was successfully deleted');
    }

    /**
     * Handle an error with user destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('users.index')->with('message', 'Oops, couldn\'t delete that user');
    }
}
