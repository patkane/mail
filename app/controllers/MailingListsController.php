<?php

use Services\MailingListCreator;
use Contracts\Repositories\MailingListRepositoryInterface;
use Contracts\Repositories\OperatorNameRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class MailingListsController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * MailingList Repository
     *
     * @var MailingListRepositoryInterface
     */
    protected $mailing_list;

    public function __construct(
        MailingListRepositoryInterface $mailing_list,
        OperatorNameRepositoryInterface $operator_name
    ) {
        $this->mailing_list = $mailing_list;
        $this->operator_name = $operator_name;
    }

    /**
     * Display a listing of the mailing_lists.
     *
     * @return Response
     */
    public function index()
    {
        $mailing_lists = $this->mailing_list->all();

        return View::make('mailing_lists.index', compact('mailing_lists'));
    }

    /**
     * Show the form for creating a new mailing_list.
     *
     * @return Response
     */
    public function create()
    {
        $operator_names = $this->operator_name->all();
        
        return View::make('mailing_lists.create', compact('operator_names'));
    }

    /**
     * Store a newly created mailing_list in storage.
     *
     * @return Response
     */
    public function store()
    {
        $mailing_list_creator = App::make('Services\MailingLists\MailingListCreator');

        return $mailing_list_creator->create($this->mailing_list, $this, Input::except('_token'));
    }

    /**
     * Handle successful mailing_list creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('mailing_lists.index')->with('message', 'MailingList was successfully created');
    }

    /**
     * Handle an error with mailing_list creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('mailing_lists.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified mailing_list.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $mailing_list = $this->mailing_list->find($id);

        return View::make('mailing_lists.show', compact('mailing_list'));
    }

    /**
     * Show the form for editing the specified mailing_list.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $mailing_list = $this->mailing_list->find($id);

        return View::make('mailing_lists.edit', compact('mailing_lists'));
    }

    /**
     * Update the specified mailing_list in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $mailing_list_updater = App::make('Services\MailingLists\MailingListUpdater');

        return $mailing_list_updater->update($this->mailing_list, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful mailing_list update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('mailing_lists.show', $instance->identity());
    }

    /**
     * Handle an error with mailing_list update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('mailing_lists.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified mailing_list from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $mailing_list_destroyer = App::make('Services\MailingLists\MailingListDestroyer');

        return $mailing_list_destroyer->destroy($this->mailing_list, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful mailing_list destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('mailing_lists.index')->with('message', 'MailingList was successfully deleted');
    }

    /**
     * Handle an error with mailing_list destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('mailing_lists.index')->with('message', 'Oops, couldn\'t delete that mailing_list');
    }
}
