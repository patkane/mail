<?php

use Services\SubscriberCreator;
use Contracts\Repositories\SubscriberRepositoryInterface;
use Contracts\Repositories\SubscriptionRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class SubscribersController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * Subscriber Repository
     *
     * @var SubscriberRepositoryInterface
     */
    protected $subscriber;

    public function __construct(
        SubscriberRepositoryInterface $subscriber,
        SubscriptionRepositoryInterface $subscription
    ) {
        $this->subscriber = $subscriber;
        $this->subscription = $subscription;
    }

    /**
     * Display a listing of the subscribers.
     *
     * @return Response
     */
    public function index()
    {
        $subscribers = $this->subscriber->all();

        return View::make('subscribers.index', compact('subscribers'));
    }

    /**
     * Show the form for creating a new subscriber.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('subscribers.create');
    }

    /**
     * Store a newly created subscriber in storage.
     *
     * @return Response
     */
    public function store()
    {
        $subscriber_creator = App::make('Services\Subscribers\SubscriberCreator');

        return $subscriber_creator->create($this->subscriber, $this, Input::except('_token'));
    }

    /**
     * Handle successful subscriber creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('subscribers.index')->with('message', 'Subscriber was successfully created');
    }

    /**
     * Handle an error with subscriber creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('subscribers.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified subscriber.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $subscriber = $this->subscriber->find($id);
        $subscriptions = array();

        return View::make('subscribers.show', compact('subscriber', 'subscriptions'));
    }

    /**
     * Show the form for editing the specified subscriber.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $subscriber = $this->subscriber->getByIdWithSubscriptionIds($id);
        $subscriptions = $this->subscription->all();

        return View::make('subscribers.edit', compact('subscriber', 'subscriptions'));
    }

    /**
     * Update the specified subscriber in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $subscriber_updater = App::make('Services\Subscribers\SubscriberUpdater');

        return $subscriber_updater->update($this->subscriber, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful subscriber update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('subscribers.show', $instance->identity());
    }

    /**
     * Handle an error with subscriber update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('subscribers.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified subscriber from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $subscriber_destroyer = App::make('Services\Subscribers\SubscriberDestroyer');

        return $subscriber_destroyer->destroy($this->subscriber, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful subscriber destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('subscribers.index')->with('message', 'Subscriber was successfully deleted');
    }

    /**
     * Handle an error with subscriber destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('subscribers.index')->with('message', 'Oops, couldn\'t delete that subscriber');
    }
}
