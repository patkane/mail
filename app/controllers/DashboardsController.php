<?php

use Services\DashboardCreator;
use Contracts\Repositories\DashboardRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class DashboardsController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * Dashboard Repository
     *
     * @var DashboardRepositoryInterface
     */
    protected $dashboard;

    public function __construct(DashboardRepositoryInterface $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Display a listing of the dashboards.
     *
     * @return Response
     */
    public function index()
    {
        $dashboards = $this->dashboard->all();

        return View::make('dashboards.index', compact('dashboards'));
    }

    /**
     * Show the form for creating a new dashboard.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('dashboards.create');
    }

    /**
     * Store a newly created dashboard in storage.
     *
     * @return Response
     */
    public function store()
    {
        $dashboard_creator = App::make('Services\Dashboards\DashboardCreator');

        return $dashboard_creator->create($this->dashboard, $this, Input::except('_token'));
    }

    /**
     * Handle successful dashboard creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('dashboards.index')->with('message', 'Dashboard was successfully created');
    }

    /**
     * Handle an error with dashboard creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('dashboards.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified dashboard.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $dashboard = $this->dashboard->find($id);

        return View::make('dashboards.show', compact('dashboard'));
    }

    /**
     * Show the form for editing the specified dashboard.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $dashboard = $this->dashboard->find($id);

        return View::make('dashboards.edit', compact('dashboards'));
    }

    /**
     * Update the specified dashboard in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $dashboard_updater = App::make('Services\Dashboards\DashboardUpdater');

        return $dashboard_updater->update($this->dashboard, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful dashboard update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('dashboards.show', $instance->identity());
    }

    /**
     * Handle an error with dashboard update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('dashboards.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified dashboard from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $dashboard_destroyer = App::make('Services\Dashboards\DashboardDestroyer');

        return $dashboard_destroyer->destroy($this->dashboard, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful dashboard destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('dashboards.index')->with('message', 'Dashboard was successfully deleted');
    }

    /**
     * Handle an error with dashboard destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('dashboards.index')->with('message', 'Oops, couldn\'t delete that dashboard');
    }
}
