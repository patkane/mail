<?php

use Contracts\Notification\LoginInterface;
use Validators\Validator as Validator;

class SessionsController extends BaseController implements LoginInterface
{

    /**
     * Display the lgin page.
     *
     * @return Response
     */
    public function showLogin()
    {
        return View::make('sessions.login');
    }

    /**
     * Process a login attempt
     *
     * @return Response
     */
    public function processLogin()
    {

        $sessions_login = App::make('Services\Sessions\SessionsLogin');

        return $sessions_login->login($this, Input::except('_token'));
    }

    /**
     * Handle successful login
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function loginSucceeded()
    {
        //return Redirect::route('sessions.index')->with('message', 'Campaign was successfully created');

        return Redirect::intended('admins');
    }

    /**
     * Handle an error with login
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function loginFailed(Validator $validator)
    {
        return Redirect::route('sessions.login')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('user', $validator->userNotFound)
            ->with('message', 'Oops, there was an error');
    }

    public function userNotFound(Validator $validator)
    {
        return Redirect::route('sessions.login')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified campaign.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $campaign = $this->campaign->find($id);

        return View::make('campaigns.show', compact('campaign'));
    }

    /**
     * Show the form for editing the specified campaign.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $campaign = $this->campaign->find($id);

        return View::make('campaigns.edit', compact('campaigns'));
    }

    /**
     * Update the specified campaign in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $campaign_updater = App::make('Services\Campaigns\CampaignUpdater');

        return $campaign_updater->update($this->campaign, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful campaign update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('campaigns.show', $instance->identity());
    }

    /**
     * Handle an error with campaign update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('campaigns.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $campaign_destroyer = App::make('Services\Campaigns\CampaignDestroyer');

        return $campaign_destroyer->destroy($this->campaign, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful campaign destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('campaigns.index')->with('message', 'Campaign was successfully deleted');
    }

    /**
     * Handle an error with campaign destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('campaigns.index')->with('message', 'Oops, couldn\'t delete that campaign');
    }
}
