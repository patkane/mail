<?php

use Services\SubscriptionCreator;
use Contracts\Repositories\SubscriptionRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class SubscriptionsController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * Subscription Repository
     *
     * @var SubscriptionRepositoryInterface
     */
    protected $subscription;

    public function __construct(SubscriptionRepositoryInterface $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Display a listing of the subscriptions.
     *
     * @return Response
     */
    public function index()
    {
        $publicSubscriptions = $this->subscription->getAllPublicWithSubscriberCount();

        return View::make('subscriptions.index', compact('publicSubscriptions'));
    }

    /**
     * Show the form for creating a new subscription.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('subscriptions.create');
    }

    /**
     * Store a newly created subscription in storage.
     *
     * @return Response
     */
    public function store()
    {
        $subscription_creator = App::make('Services\Subscriptions\SubscriptionCreator');

        return $subscription_creator->create($this->subscription, $this, Input::except('_token'));
    }

    /**
     * Handle successful subscription creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('subscriptions.index')->with('message', 'Subscription was successfully created');
    }

    /**
     * Handle an error with subscription creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('subscriptions.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified subscription.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $subscription = $this->subscription->find($id);

        return View::make('subscriptions.show', compact('subscription'));
    }

    /**
     * Show the form for editing the specified subscription.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $subscription = $this->subscription->find($id);

        return View::make('subscriptions.edit', compact('subscriptions'));
    }

    /**
     * Update the specified subscription in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $subscription_updater = App::make('Services\Subscriptions\SubscriptionUpdater');

        return $subscription_updater->update($this->subscription, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful subscription update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('subscriptions.show', $instance->identity());
    }

    /**
     * Handle an error with subscription update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('subscriptions.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified subscription from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $subscription_destroyer = App::make('Services\Subscriptions\SubscriptionDestroyer');

        return $subscription_destroyer->destroy($this->subscription, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful subscription destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('subscriptions.index')->with('message', 'Subscription was successfully deleted');
    }

    /**
     * Handle an error with subscription destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('subscriptions.index')->with('message', 'Oops, couldn\'t delete that subscription');
    }
}
