<?php

use Services\TeamCreator;
use Contracts\Repositories\TeamRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class TeamsController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * Team Repository
     *
     * @var TeamRepositoryInterface
     */
    protected $team;

    public function __construct(TeamRepositoryInterface $team)
    {
        $this->team = $team;
    }

    /**
     * Display a listing of the teams.
     *
     * @return Response
     */
    public function index()
    {
        $teams = $this->team->all();

        return View::make('teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new team.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('teams.create');
    }

    /**
     * Store a newly created team in storage.
     *
     * @return Response
     */
    public function store()
    {
        $team_creator = App::make('Services\Teams\TeamCreator');

        return $team_creator->create($this->team, $this, Input::except('_token'));
    }

    /**
     * Handle successful team creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('teams.index')->with('message', 'Team was successfully created');
    }

    /**
     * Handle an error with team creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('teams.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified team.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $team = $this->team->find($id);

        return View::make('teams.show', compact('team'));
    }

    /**
     * Show the form for editing the specified team.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $team = $this->team->find($id);

        return View::make('teams.edit', compact('teams'));
    }

    /**
     * Update the specified team in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $team_updater = App::make('Services\Teams\TeamUpdater');

        return $team_updater->update($this->team, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful team update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('teams.show', $instance->identity());
    }

    /**
     * Handle an error with team update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('teams.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified team from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $team_destroyer = App::make('Services\Teams\TeamDestroyer');

        return $team_destroyer->destroy($this->team, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful team destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('teams.index')->with('message', 'Team was successfully deleted');
    }

    /**
     * Handle an error with team destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('teams.index')->with('message', 'Oops, couldn\'t delete that team');
    }
}
