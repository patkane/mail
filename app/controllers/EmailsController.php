<?php

use Services\EmailCreator;
use Contracts\Repositories\EmailRepositoryInterface;
use Contracts\Repositories\SubscriptionRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class EmailsController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * Email Repository
     *
     * @var EmailRepositoryInterface
     */
    protected $email;

    public function __construct(
        EmailRepositoryInterface $email,
        SubscriptionRepositoryInterface $subscription
    ) {
        $this->email = $email;
        $this->subscription = $subscription;
    }

    /**
     * Display a listing of the emails.
     *
     * @return Response
     */
    public function index()
    {
        $emails = $this->email->all();

        return View::make('emails.index', compact('emails'));
    }

    /**
     * Show the form for creating a new email.
     *
     * @return Response
     */
    public function create()
    {
        $subscriptions = $this->subscription->all();
        return View::make('emails.create', compact('subscriptions'));
    }

    /**
     * Store a newly created email in storage.
     *
     * @return Response
     */
    public function store()
    {
        $email_creator = App::make('Services\Emails\EmailCreator');

        return $email_creator->create($this->email, $this, Input::except('_token'));
    }

    /**
     * Handle successful email creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('emails.index')->with('message', 'Email was successfully created');
    }

    /**
     * Handle an error with email creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('emails.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified email.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $email = $this->email->find($id);

        return View::make('emails.show', compact('email'));
    }

    /**
     * Show the form for editing the specified email.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $email = $this->email->find($id);
        $subscriptions = $this->subscription->all();

        return View::make('emails.edit', compact('email', 'subscriptions'));
    }

    /**
     * Update the specified email in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $email_updater = App::make('Services\Emails\EmailUpdater');

        return $email_updater->update($this->email, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful email update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('emails.show', $instance->identity());
    }

    /**
     * Handle an error with email update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('emails.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified email from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $email_destroyer = App::make('Services\Emails\EmailDestroyer');

        return $email_destroyer->destroy($this->email, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful email destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('emails.index')->with('message', 'Email was successfully deleted');
    }

    /**
     * Handle an error with email destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('emails.index')->with('message', 'Oops, couldn\'t delete that email');
    }
}
