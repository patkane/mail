<?php

class AdminsController extends BaseController
{

    /**
     * Display a listing of the admins.
     *
     * @return Response
     */
    public function index()
    {

        return View::make('admins.index');
    }
}
