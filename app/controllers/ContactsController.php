<?php

use Services\ContactCreator;
use Contracts\Repositories\ContactRepositoryInterface;
use Contracts\Repositories\SubscriptionRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class ContactsController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * Contact Repository
     *
     * @var ContactRepositoryInterface
     */
    protected $contact;

    public function __construct(
        ContactRepositoryInterface $contact,
        SubscriptionRepositoryInterface $subscription
    ) {
        $this->contact = $contact;
        $this->subscription = $subscription;
    }

    /**
     * Display a listing of the contacts.
     *
     * @return Response
     */
    public function index()
    {
        $contacts = $this->contact->all();

        return View::make('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new contact.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('contacts.create');
    }

    /**
     * Store a newly created contact in storage.
     *
     * @return Response
     */
    public function store()
    {
        $contact_creator = App::make('Services\Contacts\ContactCreator');

        return $contact_creator->create($this->contact, $this, Input::except('_token'));
    }

    /**
     * Handle successful contact creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('contacts.index')->with('message', 'Contact was successfully created');
    }

    /**
     * Handle an error with contact creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('contacts.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified contact.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $contact = $this->contact->getByIdWithSubscriptionNames($id);

        return View::make('contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified contact.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $contact = $this->contact->getByIdWithSubscriptionIds($id);
        $subscriptions = $this->subscription->all();

        return View::make('contacts.edit', compact('contact', 'subscriptions'));
    }

    /**
     * Update the specified contact in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $contact_updater = App::make('Services\Contacts\ContactUpdater');

        return $contact_updater->update($this->contact, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful contact update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('contacts.show', $instance->identity());
    }

    /**
     * Handle an error with contact update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('contacts.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified contact from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $contact_destroyer = App::make('Services\Contacts\ContactDestroyer');

        return $contact_destroyer->destroy($this->contact, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful contact destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('contacts.index')->with('message', 'Contact was successfully deleted');
    }

    /**
     * Handle an error with contact destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('contacts.index')->with('message', 'Oops, couldn\'t delete that contact');
    }

    public function getImport()
    {
        return 'test';
    }
}
