<?php

use Services\CampaignCreator;
use Contracts\Repositories\CampaignRepositoryInterface;
use Contracts\Instances\InstanceInterface;
use Contracts\Notification\CreatorInterface;
use Contracts\Notification\UpdaterInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\Validator as Validator;

class CampaignsController extends BaseController implements CreatorInterface, UpdaterInterface, DestroyerInterface
{

    /**
     * Campaign Repository
     *
     * @var CampaignRepositoryInterface
     */
    protected $campaign;

    public function __construct(CampaignRepositoryInterface $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Display a listing of the campaigns.
     *
     * @return Response
     */
    public function index()
    {
        $campaigns = $this->campaign->all();

        return View::make('campaigns.index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('campaigns.create');
    }

    /**
     * Store a newly created campaign in storage.
     *
     * @return Response
     */
    public function store()
    {
        $campaign_creator = App::make('Services\Campaigns\CampaignCreator');

        return $campaign_creator->create($this->campaign, $this, Input::except('_token'));
    }

    /**
     * Handle successful campaign creation
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function creationSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('campaigns.index')->with('message', 'Campaign was successfully created');
    }

    /**
     * Handle an error with campaign creation
     *
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function creationFailed(Validator $validator)
    {
        return Redirect::route('campaigns.create')
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Display the specified campaign.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $campaign = $this->campaign->find($id);

        return View::make('campaigns.show', compact('campaign'));
    }

    /**
     * Show the form for editing the specified campaign.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $campaign = $this->campaign->find($id);

        return View::make('campaigns.edit', compact('campaigns'));
    }

    /**
     * Update the specified campaign in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $campaign_updater = App::make('Services\Campaigns\CampaignUpdater');

        return $campaign_updater->update($this->campaign, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful campaign update
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function updateSucceeded(InstanceInterface $instance)
    {
        return Redirect::route('campaigns.show', $instance->identity());
    }

    /**
     * Handle an error with campaign update
     *
     * @param  InstanceInterface $instance
     * @param  Validator\Validator      $validator
     * @return Redirect::route
     */
    public function updateFailed(InstanceInterface $instance, Validator $validator)
    {
        return Redirect::route('campaigns.edit', $instance->identity())
            ->withInput()
            ->withErrors($validator->errors())
            ->with('message', 'Oops, there was an error');
    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $campaign_destroyer = App::make('Services\Campaigns\CampaignDestroyer');

        return $campaign_destroyer->destroy($this->campaign, $this, $id, Input::except('_method'));
    }

    /**
     * Handle successful campaign destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroySucceeded(InstanceInterface $instance)
    {
        return Redirect::route('campaigns.index')->with('message', 'Campaign was successfully deleted');
    }

    /**
     * Handle an error with campaign destroy
     *
     * @param  InstanceInterface $instance
     * @return Redirect::route
     */
    public function destroyFailed(InstanceInterface $instance)
    {
        return Redirect::route('campaigns.index')->with('message', 'Oops, couldn\'t delete that campaign');
    }
}
