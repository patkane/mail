<?php

namespace Mail;

use Contracts\Instances\InstanceInterface;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Contact extends Eloquent implements InstanceInterface
{
    protected $guarded = [];

    public function identity()
    {
        return $this->id;
    }

    public function subscriptions()
    {
        return $this->belongsToMany('Mail\Subscription', 'contacts_subscriptions');
    }
}
