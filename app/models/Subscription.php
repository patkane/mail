<?php

namespace Mail;

use Contracts\Instances\InstanceInterface;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Subscription extends Eloquent implements InstanceInterface
{

    protected $guarded = [];

    public function identity()
    {
        return $this->id;
    }
    
    protected $fillable = [];

    public function subscribers()
    {
        return $this->belongsToMany('Subscriber');
    }
}
