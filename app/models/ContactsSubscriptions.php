<?php

namespace Mail;

use Contracts\Instances\InstanceInterface;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ContactsSubscriptions extends Eloquent implements InstanceInterface
{

    protected $guarded = [];

    public function identity()
    {
        return $this->id;
    }
    protected $fillable = [];

    protected $table = 'contacts_subscriptions';
}
