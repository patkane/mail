<?php

namespace Mail;

use Contracts\Instances\InstanceInterface;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Campaign extends Eloquent implements InstanceInterface
{

    protected $guarded = [];

    public function identity()
    {
        return $this->id;
    }

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = [];
}
