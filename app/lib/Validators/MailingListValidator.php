<?php namespace Validators;

class MailingListValidator extends Validator
{
    /**
     * Array of validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
}
