<?php namespace Validators;

class ContactValidator extends Validator
{
    /**
     * Array of validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|email|unique:contacts,email,{{ $id }}',
        'first_name' => 'required',
        'last_name' => 'required',
        'address' => '',
        'secondary_address' => '',
        'city' => '',
        'state' => '',
        'country' => '',
        'gender' => '',
        'shirt_size' => '',
        'birthdate' => 'date',
        'phone' => ''
    ];
}
