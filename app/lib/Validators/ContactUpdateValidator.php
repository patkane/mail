<?php namespace Validators;

class ContactUpdateValidator extends Validator
{
    /**
     * Array of validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|email',
        'first_name' => 'required',
        'last_name' => 'required',
        'address' => '',
        'secondary_address' => '',
        'city' => '',
        'state' => '',
        'country' => '',
        'gender' => '',
        'shirt_size' => '',
        'birthdate' => 'date',
        'phone' => ''
    ];
}
