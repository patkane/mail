<?php namespace Validators;

class EmailValidator extends Validator
{
    /**
     * Array of validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:emails,name',
        'subject' => 'required',
        'from_name' => 'required',
        'from_email' => 'required|email',
        'reply_to_email' => 'required|email',
        'send_date' => 'required_if:status,active|date',
        'send_time' => 'required_if:status,active|time',
        'status' => 'required',
        'body' => 'required'
        
    ];
}
