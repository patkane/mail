<?php namespace Validators;

class UserValidator extends Validator
{
    /**
     * Array of validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|email',
        'password' => 'required'
    ];
}
