<?php namespace Services\MailingLists;

use Contracts\Repositories\MailingListRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\MailingListValidator;

class MailingListDestroyer
{

    /**
     * Attempt to destroy the mailing_list and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  MailingListRepositoryInterface $mailing_list
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(MailingListRepositoryInterface $mailing_list, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $mailing_list->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
