<?php namespace Services\MailingLists;

use Contracts\Repositories\MailingListRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\MailingListValidator;

class MailingListUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param MailingListValidator $validator
     */
    public function __construct(MailingListValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the mailing_list with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  MailingListRepositoryInterface $mailing_list
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(MailingListRepositoryInterface $mailing_list, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $mailing_list->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
