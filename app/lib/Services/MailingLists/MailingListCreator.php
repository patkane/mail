<?php namespace Services\MailingLists;

use Contracts\Repositories\MailingListRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\MailingListValidator;

class MailingListCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param MailingListValidator $validator
     */
    public function __construct(MailingListValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new mailing_list with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  MailingListRepositoryInterface $mailing_list     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(MailingListRepositoryInterface $mailing_list, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $mailing_list->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
