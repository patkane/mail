<?php namespace Services\Teams;

use Contracts\Repositories\TeamRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\TeamValidator;

class TeamUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param TeamValidator $validator
     */
    public function __construct(TeamValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the team with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  TeamRepositoryInterface $team
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(TeamRepositoryInterface $team, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $team->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
