<?php namespace Services\Teams;

use Contracts\Repositories\TeamRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\TeamValidator;

class TeamDestroyer
{

    /**
     * Attempt to destroy the team and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  TeamRepositoryInterface $team
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(TeamRepositoryInterface $team, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $team->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
