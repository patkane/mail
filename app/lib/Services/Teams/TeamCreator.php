<?php namespace Services\Teams;

use Contracts\Repositories\TeamRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\TeamValidator;

class TeamCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param TeamValidator $validator
     */
    public function __construct(TeamValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new team with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  TeamRepositoryInterface $team     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(TeamRepositoryInterface $team, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $team->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
