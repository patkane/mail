<?php namespace Services\Subscriptions;

use Contracts\Repositories\SubscriptionRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\SubscriptionValidator;

class SubscriptionDestroyer
{

    /**
     * Attempt to destroy the subscription and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  SubscriptionRepositoryInterface $subscription
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(SubscriptionRepositoryInterface $subscription, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $subscription->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
