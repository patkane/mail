<?php namespace Services\Subscriptions;

use Contracts\Repositories\SubscriptionRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\SubscriptionValidator;

class SubscriptionCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param SubscriptionValidator $validator
     */
    public function __construct(SubscriptionValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new subscription with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  SubscriptionRepositoryInterface $subscription     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(SubscriptionRepositoryInterface $subscription, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $subscription->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
