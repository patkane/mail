<?php namespace Services\Subscriptions;

use Contracts\Repositories\SubscriptionRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\SubscriptionValidator;

class SubscriptionUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param SubscriptionValidator $validator
     */
    public function __construct(SubscriptionValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the subscription with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  SubscriptionRepositoryInterface $subscription
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(SubscriptionRepositoryInterface $subscription, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $subscription->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
