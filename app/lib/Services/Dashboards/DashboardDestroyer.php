<?php namespace Services\Dashboards;

use Contracts\Repositories\DashboardRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\DashboardValidator;

class DashboardDestroyer
{

    /**
     * Attempt to destroy the dashboard and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  DashboardRepositoryInterface $dashboard
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(DashboardRepositoryInterface $dashboard, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $dashboard->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
