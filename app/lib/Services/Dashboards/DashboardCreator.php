<?php namespace Services\Dashboards;

use Contracts\Repositories\DashboardRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\DashboardValidator;

class DashboardCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param DashboardValidator $validator
     */
    public function __construct(DashboardValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new dashboard with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  DashboardRepositoryInterface $dashboard     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(DashboardRepositoryInterface $dashboard, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $dashboard->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
