<?php namespace Services\Dashboards;

use Contracts\Repositories\DashboardRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\DashboardValidator;

class DashboardUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param DashboardValidator $validator
     */
    public function __construct(DashboardValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the dashboard with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  DashboardRepositoryInterface $dashboard
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(DashboardRepositoryInterface $dashboard, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $dashboard->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
