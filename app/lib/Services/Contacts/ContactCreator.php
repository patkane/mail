<?php namespace Services\Contacts;

use Contracts\Repositories\ContactRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\ContactValidator;

class ContactCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param ContactValidator $validator
     */
    public function __construct(ContactValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new contact with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  ContactRepositoryInterface $contact     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(ContactRepositoryInterface $contact, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $contact->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
