<?php namespace Services\Contacts;

use Contracts\Repositories\ContactRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\ContactValidator;

class ContactDestroyer
{

    /**
     * Attempt to destroy the contact and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  ContactRepositoryInterface $contact
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(ContactRepositoryInterface $contact, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $contact->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
