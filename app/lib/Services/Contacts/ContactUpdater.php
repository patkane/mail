<?php namespace Services\Contacts;

use Contracts\Repositories\ContactRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\ContactUpdateValidator;

class ContactUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param ContactValidator $validator
     */
    public function __construct(ContactUpdateValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the contact with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  ContactRepositoryInterface $contact
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(ContactRepositoryInterface $contact, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $contact->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
