<?php namespace Services\Emails;

use Contracts\Repositories\EmailRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\EmailValidator;

class EmailUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param EmailValidator $validator
     */
    public function __construct(EmailValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the email with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  EmailRepositoryInterface $email
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(EmailRepositoryInterface $email, UpdaterInterface $listener, $identity, array $attributes = [])
    {

        $instance = $email->find($identity);

        if ($this->validator->validate($attributes)) {
            $attributes['send_date'] = date('Y-m-d', strtotime($attributes['send_date']));
            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
