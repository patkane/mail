<?php namespace Services\Emails;

use Contracts\Repositories\EmailRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\EmailValidator;

class EmailCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param EmailValidator $validator
     */
    public function __construct(EmailValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new email with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  EmailRepositoryInterface $email     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(EmailRepositoryInterface $email, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $email->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
