<?php namespace Services\Emails;

use Contracts\Repositories\EmailRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\EmailValidator;

class EmailDestroyer
{

    /**
     * Attempt to destroy the email and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  EmailRepositoryInterface $email
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(EmailRepositoryInterface $email, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $email->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
