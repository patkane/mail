<?php namespace Services\Admins;

use Contracts\Repositories\AdminRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\AdminValidator;

class AdminDestroyer
{

    /**
     * Attempt to destroy the admin and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  AdminRepositoryInterface $admin
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(AdminRepositoryInterface $admin, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $admin->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
