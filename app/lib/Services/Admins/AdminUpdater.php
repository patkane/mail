<?php namespace Services\Admins;

use Contracts\Repositories\AdminRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\AdminValidator;

class AdminUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param AdminValidator $validator
     */
    public function __construct(AdminValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the admin with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  AdminRepositoryInterface $admin
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(AdminRepositoryInterface $admin, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $admin->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
