<?php namespace Services\Admins;

use Contracts\Repositories\AdminRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\AdminValidator;

class AdminCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param AdminValidator $validator
     */
    public function __construct(AdminValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new admin with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  AdminRepositoryInterface $admin     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(AdminRepositoryInterface $admin, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $admin->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
