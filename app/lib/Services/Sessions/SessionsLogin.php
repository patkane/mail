<?php namespace Services\Sessions;

use Contracts\Repositories\UserRepositoryInterface;
use Contracts\Notification\LoginInterface;
use Validators\UserValidator;
use Auth;

class SessionsLogin
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param RoleValidator $validator
     */
    public function __construct(UserValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to 
     * 
     * @param  RoleRepositoryInterface $use     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function login(LoginInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            if (Auth::attempt(array(
                'email' => $attributes['email'],
                'password' => $attributes['password'],
                'status' => 1
            ))) {
                return $listener->loginSucceeded();
            } else {
                return $listener->loginFailed($this->validator);
            }
        } else {

            return $listener->loginFailed($this->validator);
        }
    }
}
