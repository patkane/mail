<?php namespace Services\Campaigns;

use Contracts\Repositories\CampaignRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\CampaignValidator;

class CampaignDestroyer
{

    /**
     * Attempt to destroy the campaign and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  CampaignRepositoryInterface $campaign
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(CampaignRepositoryInterface $campaign, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $campaign->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
