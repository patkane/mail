<?php namespace Services\Campaigns;

use Contracts\Repositories\CampaignRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\CampaignValidator;

class CampaignCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param CampaignValidator $validator
     */
    public function __construct(CampaignValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new campaign with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  CampaignRepositoryInterface $campaign     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(CampaignRepositoryInterface $campaign, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $campaign->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
