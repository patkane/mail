<?php namespace Services\Campaigns;

use Contracts\Repositories\CampaignRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\CampaignValidator;

class CampaignUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param CampaignValidator $validator
     */
    public function __construct(CampaignValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the campaign with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  CampaignRepositoryInterface $campaign
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(CampaignRepositoryInterface $campaign, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $campaign->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
