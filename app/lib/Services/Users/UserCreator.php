<?php namespace Services\Users;

use Contracts\Repositories\UserRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\UserValidator;

class UserCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param UserValidator $validator
     */
    public function __construct(UserValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new user with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  UserRepositoryInterface $user     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(UserRepositoryInterface $user, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $user->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
