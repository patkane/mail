<?php namespace Services\Users;

use Contracts\Repositories\UserRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\UserValidator;

class UserDestroyer
{

    /**
     * Attempt to destroy the user and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  UserRepositoryInterface $user
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(UserRepositoryInterface $user, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $user->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
