<?php namespace Services\Users;

use Contracts\Repositories\UserRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\UserValidator;

class UserUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param UserValidator $validator
     */
    public function __construct(UserValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the user with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  UserRepositoryInterface $user
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(UserRepositoryInterface $user, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $user->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
