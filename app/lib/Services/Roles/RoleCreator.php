<?php namespace Services\Roles;

use Contracts\Repositories\RoleRepositoryInterface;
use Contracts\Notification\CreatorInterface;
use Validators\RoleValidator;

class RoleCreator
{

    protected $validator;


    /**
     * Inject the validator that will be used for
     * creation
     * 
     * @param RoleValidator $validator
     */
    public function __construct(RoleValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to create a new role with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  RoleRepositoryInterface $role     
     * @param  CreatorInterface         $listener  
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener                        
     */
    public function create(RoleRepositoryInterface $role, CreatorInterface $listener, array $attributes = [])
    {
        if ($this->validator->validate($attributes)) {

            $instance = $role->create($attributes);
            
            return $listener->creationSucceeded($instance);

        } else {

            return $listener->creationFailed($this->validator);
        }
    }
}
