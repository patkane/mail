<?php namespace Services\Roles;

use Contracts\Repositories\RoleRepositoryInterface;
use Contracts\Notification\DestroyerInterface;
use Validators\RoleValidator;

class RoleDestroyer
{

    /**
     * Attempt to destroy the role and
     * notify the $listener of the success or failure.  The
     * $attributes are passed in as a convenience in case they
     * are needed
     * 
     * @param  RoleRepositoryInterface $role
     * @param  DestroyerInterface       $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function destroy(RoleRepositoryInterface $role, DestroyerInterface $listener, $identity, array $attributes = [])
    {
        $instance = $role->find($identity);

        if ($instance->delete()) {

            return $listener->destroySucceeded($instance);

        } else {

            return $listener->destroyFailed($instance);
        }
    }
}
