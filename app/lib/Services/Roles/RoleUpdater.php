<?php namespace Services\Roles;

use Contracts\Repositories\RoleRepositoryInterface;
use Contracts\Notification\UpdaterInterface;
use Validators\RoleValidator;

class RoleUpdater
{

    protected $validator;

    /**
     * Inject the validator used for updating
     * 
     * @param RoleValidator $validator
     */
    public function __construct(RoleValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Attempt to update the role with the given attributes and
     * notify the $listener of the success or failure
     * 
     * @param  RoleRepositoryInterface $role
     * @param  UpdaterInterface         $listener 
     * @param  mixed                    $identity
     * @param  array                    $attributes
     * @return mixed - returned value from the $listener 
     */
    public function update(RoleRepositoryInterface $role, UpdaterInterface $listener, $identity, array $attributes = [])
    {
        $instance = $role->find($identity);

        if ($this->validator->validate($attributes)) {

            $instance->update($attributes);

            return $listener->updateSucceeded($instance);

        } else {

            return $listener->updateFailed($instance, $this->validator);
        }
    }
}
