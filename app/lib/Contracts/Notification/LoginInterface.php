<?php namespace Contracts\Notification;

use Validators\Validator;
// /use Contracts\Instances\InstanceInterface;

interface LoginInterface
{
    public function loginSucceeded(); //InstanceInterface $instance

    public function loginFailed(Validator $validator);

    //public function userNotFound(Validator $validator);
}
