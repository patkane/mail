<?php namespace Repositories;

use Contracts\Repositories\OperatorNameRepositoryInterface;
use Mail\OperatorName;

class DbOperatorNameRepository extends DbRepository implements OperatorNameRepositoryInterface
{
    public function __construct(OperatorName $model)
    {
        $this->model = $model;
    }
}
