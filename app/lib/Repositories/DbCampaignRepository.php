<?php namespace Repositories;

use Contracts\Repositories\CampaignRepositoryInterface;
use Mail\Campaign;

class DbCampaignRepository extends DbRepository implements CampaignRepositoryInterface
{
    public function __construct(Campaign $model)
    {
        $this->model = $model;
    }
}
