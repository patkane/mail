<?php namespace Repositories;

use Contracts\Repositories\UserRepositoryInterface;
use Mail\User;

class DbUserRepository extends DbRepository implements UserRepositoryInterface
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }
}
