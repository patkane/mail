<?php namespace Repositories;

use Contracts\Repositories\EmailRepositoryInterface;
use Mail\Email;

class DbEmailRepository extends DbRepository implements EmailRepositoryInterface
{
    public function __construct(Email $model)
    {
        $this->model = $model;
    }
}
