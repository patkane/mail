<?php namespace Repositories;

use Contracts\Repositories\ContactRepositoryInterface;
use Mail\Contact;

class DbContactRepository extends DbRepository implements ContactRepositoryInterface
{
    public function __construct(Contact $model)
    {
        $this->model = $model;
    }

    public function getByIdWithSubscriptionIds($id)
    {
        $contact = $this->model->with('subscriptions')->where('id', $id)->first();
        $subscriptions = array();
        foreach ($contact->subscriptions as $s) {
            array_push($subscriptions, $s->pivot->subscription_id);
        }
        unset($contact->subscriptions);
        $contact['subscriptions'] = $subscriptions;

        return $contact;
    }

    public function getByIdWithSubscriptionNames($id)
    {
        return $this->model->with('subscriptions')->where('id', $id)->first();

    }
}
