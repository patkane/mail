<?php namespace Repositories;

use Contracts\Repositories\MailingListRepositoryInterface;
use Mail\MailingList;

class DbMailingListRepository extends DbRepository implements MailingListRepositoryInterface
{
    public function __construct(MailingList $model)
    {
        $this->model = $model;
    }
}
