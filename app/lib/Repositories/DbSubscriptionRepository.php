<?php namespace Repositories;

use Contracts\Repositories\SubscriptionRepositoryInterface;
use Mail\Subscription;
use DB;

class DbSubscriptionRepository extends DbRepository implements SubscriptionRepositoryInterface
{
    public function __construct(Subscription $model)
    {
        $this->model = $model;
    }

    public function getAllPublicWithSubscriberCount()
    {
        return DB::table('subscriptions')->select(
            'subscriptions.id',
            'subscriptions.name',
            DB::raw('COUNT( contacts_subscriptions.subscription_id ) AS total_subscribers')
        )
            ->leftJoin('contacts_subscriptions', 'subscriptions.id', '=', 'contacts_subscriptions.subscription_id')
            ->where('type', 'public')
            ->groupBy('subscriptions.id')
            ->orderBy('total_subscribers', 'desc')
            ->get();
        
    }
}
