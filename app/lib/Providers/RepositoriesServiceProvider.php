<?php namespace Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'Contracts\Repositories\OrderRepositoryInterface',
            'Repositories\DbOrderRepository'
        );

        $this->app->bind(
            'Contracts\Repositories\CampaignRepositoryInterface',
            'Repositories\DbCampaignRepository'
        );

        $this->app->bind(
            'Contracts\Repositories\UserRepositoryInterface',
            'Repositories\DbUserRepository'
        );

        $this->app->bind(
            'Contracts\Repositories\EmailRepositoryInterface',
            'Repositories\DbEmailRepository'
        );

        $this->app->bind(
            'Contracts\Repositories\ContactRepositoryInterface',
            'Repositories\DbContactRepository'
        );

        $this->app->bind(
            'Contracts\Repositories\SubscriptionRepositoryInterface',
            'Repositories\DbSubscriptionRepository'
        );

        $this->app->bind(
            'Contracts\Repositories\MailingListRepositoryInterface',
            'Repositories\DbMailingListRepository'
        );

        $this->app->bind(
            'Contracts\Repositories\OperatorNameRepositoryInterface',
            'Repositories\DbOperatorNameRepository'
        );
    }
}
