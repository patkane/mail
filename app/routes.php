<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return Redirect::to('/admins');
});

Route::get('login', array('as' => 'sessions.login', 'uses' => 'SessionsController@showLogin'));
Route::post('login', array('as' => 'sessions.login', 'uses' => 'SessionsController@processLogin'));

Route::resource('admins', 'AdminsController');

Route::resource('campaigns', 'CampaignsController');

Route::resource('dashboards', 'DashboardsController');

Route::resource('emails', 'EmailsController');

Route::resource('roles', 'RolesController');

Route::resource('subscribers', 'SubscribersController');

Route::resource('subscriptions', 'SubscriptionsController');

Route::resource('users', 'UsersController');

Route::resource('teams', 'TeamsController');

Route::get('contacts/import', array('as' => 'contacts.import', 'uses' => 'ContactsController@getImport'));
Route::post('contacts/import', array('as' => 'contacts.import', 'uses' => 'ContactsController@postImport'));
Route::resource('contacts', 'ContactsController');


Route::resource('mailing_lists', 'MailingListsController');
