<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mail</title>

        <!-- Bootstrap -->
        <link href= {{ URL::asset('assets/css/bootstrap.min.css') }} rel="stylesheet">
        <link href= {{ URL::asset('assets/css/font-awesome.min.css') }} rel="stylesheet">
        <!--<link href= {{ URL::asset('assets/css/index.css') }} rel="stylesheet"> -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script> 

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        @yield('headerScripts')
    </head>
    <body>
        <div class="container">

            @section('nav')

                <ul class="nav nav-tabs navbar-static-top" role="navigation">
                    <li><a href="{{ URL::to('contacts') }}">Contacts <i class="fa fa-male"></i><i class="fa fa-female"></i></a></li>
                    <li><a href="{{ URL::to('mailing_lists') }}">Mailing Lists <i class="fa fa-th-list"></i></a></li>
                    <li><a href="{{ URL::to('emails') }}">Emails <i class="fa fa-envelope"></i></a></li>
                    <li><a href="{{ URL::to('subscriptions') }}">Subscriptions <i class="fa fa-rss"></i></a></li>
                    <li><a href="{{ URL::to('surveys') }}">Surveys <i class="fa fa-star"></i></a></li>
                    <li><a href="{{ URL::to('campaigns') }}">Campaigns <i class="fa fa-inbox"></i></a></li>
                    <li><a href="{{ URL::to('stats') }}">Statistics <i class="fa fa-bar-chart-o"></i></a></li>
                    <li><a href="{{ URL::to('reports') }}">Reports <i class="fa fa-file-text"></i></a></li>
                    <li><a href="{{ URL::to('admins') }}">Admin <i class="fa fa-gears"></i></a></li>
                </ul>

            @show
        </div>
        <div class="container">

        @yield('main')
            
        </div>
        @yield('bodyScripts')
    </body>
</html>