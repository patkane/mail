@extends('layouts.scaffold')

@section('main')

<h1>Edit Dashboard</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($dashboard, array('method' => 'PATCH', 'route' => array('dashboards.update', $dashboard->id))) }}
    <ul>
        
        @include('dashboards.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('dashboards.show', 'Cancel', $dashboard->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
