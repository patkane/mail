@extends('layouts.scaffold')

@section('main')

<h1>Show Dashboard</h1>

<p>{{ link_to_route('dashboards.index', 'Return to all dashboards') }}</p>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
        </tr>
    </thead>

    <tbody>
        <tr>

            <td>{{ link_to_route('dashboards.edit', 'Edit', array($dashboard->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('dashboards.destroy', $dashboard->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
