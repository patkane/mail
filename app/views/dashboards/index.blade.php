@extends('layouts.scaffold')

@section('main')

<h1>All Dashboards</h1>

<p>{{ link_to_route('dashboards.create', 'Add new ') }}</p>

@if ($dashboards->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
            </tr>
        </thead>

        <tbody>
            @foreach ($dashboards as $dashboard)
                <tr>

                    <td>{{ link_to_route('dashboards.show', 'View', array($dashboard->id), array('class' => 'btn')) }}</td>

                    <td>{{ link_to_route('dashboards.edit', 'Edit', array($dashboard->id), array('class' => 'btn btn-info')) }}</td>

                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('dashboards.destroy', $dashboard->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no dashboards
@endif

@stop
