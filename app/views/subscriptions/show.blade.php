@extends('layouts.master')

@section('main')

<h1>Show Subscription</h1>

<p>{{ link_to_route('subscriptions.index', 'Return to all subscriptions') }}</p>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Name</th>
        </tr>
    </thead>

    <tbody>
        <tr>

            <td>{{ link_to_route('subscriptions.edit', 'Edit', array($subscription->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('subscriptions.destroy', $subscription->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
