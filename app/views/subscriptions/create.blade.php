@extends('layouts.scaffold')

@section('main')

<h1>Create Subscription</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::open(array('route' => 'subscriptions.store')) }}
    <ul>
        
        @include('subscriptions.form')

        <li>
            {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
        </li>
    </ul>
{{ Form::close() }}

@stop


