@extends('layouts.master')

@section('main')

<h1>All Subscriptions</h1>

<p>{{ link_to_route('subscriptions.create', 'Add new ') }}</p>

@if (!empty($publicSubscriptions))
    <div class="row">
            <div id="public" class="col-xs-12 col-md-4">
                <button id="add_new" type="button" class="btn btn-success">Create Subscription</button>
                <h3>Public Subscriptions
                </br><small>Users can choose to subscribe or unsubscribe</small></h3>
                <table class="table" >
                    <tr>
                        <th>Subscription</th>

                    </tr>

                    @foreach ($publicSubscriptions as $publicSubscription)
                        <tr>
                            <td>
                                <a href="subscriptions/<?php echo $publicSubscription->id ?>">{{  $publicSubscription->name  }}  
                                    <span class="badge pull-right">{{  $publicSubscription->total_subscribers  }} Subscribers</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
@else
    There are no subscriptions
@endif

@stop
