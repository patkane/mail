@extends('layouts.scaffold')

@section('main')

<h1>Edit Subscription</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($subscription, array('method' => 'PATCH', 'route' => array('subscriptions.update', $subscription->id))) }}
    <ul>
        
        @include('subscriptions.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('subscriptions.show', 'Cancel', $subscription->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
