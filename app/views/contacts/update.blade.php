@extends('layouts.master')

@section('main')

<h1>Edit Contact</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($contact, array('method' => 'PATCH', 'route' => array('contacts.update', $contact->id))) }}
    <ul>
        
        @include('contacts.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('contacts.show', 'Cancel', $contact->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
