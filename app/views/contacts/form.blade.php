<div class="row">
    <div class="col-xs-12 col-md-10 well">
        <div class="form-group">
            {{ Form::label('email', 'Email', array('class'=> 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::email('email', null, array('class'=> 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('first_name', 'First Name', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('first_name', null, array('class'=> 'form-control')) }}
            </div>
            
            {{ Form::label('last_name', 'Last Name', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('last_name', null, array('class'=> 'form-control')) }}
            </div>
        </div>

        </br>
        </br>
        </br>

        <div class="form-group">
            {{ Form::label('address', 'Address', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('address', null, array('class'=> 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('secondary_address', 'Address 2', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('secondary_address', null, array('class'=> 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('city', 'City', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('city', null, array('class'=> 'form-control')) }}
            </div>

            {{ Form::label('state', 'State', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::select('state', array(
                    'AL'=>'Alabama',
                    'AK'=>'Alaska',
                    'AZ'=>'Arizona',
                    'AR'=>'Arkansas',
                    'CA'=>'California',
                    'CO'=>'Colorado',
                    'CT'=>'Connecticut',
                    'DE'=>'Delaware',
                    'DC'=>'District of Columbia',
                    'FL'=>'Florida',
                    'GA'=>'Georgia',
                    'HI'=>'Hawaii',
                    'ID'=>'Idaho',
                    'IL'=>'Illinois',
                    'IN'=>'Indiana',
                    'IA'=>'Iowa',
                    'KS'=>'Kansas',
                    'KY'=>'Kentucky',
                    'LA'=>'Louisiana',
                    'ME'=>'Maine',
                    'MD'=>'Maryland',
                    'MA'=>'Massachusetts',
                    'MI'=>'Michigan',
                    'MN'=>'Minnesota',
                    'MS'=>'Mississippi',
                    'MO'=>'Missouri',
                    'MT'=>'Montana',
                    'NE'=>'Nebraska',
                    'NV'=>'Nevada',
                    'NH'=>'New Hampshire',
                    'NJ'=>'New Jersey',
                    'NM'=>'New Mexico',
                    'NY'=>'New York',
                    'NC'=>'North Carolina',
                    'ND'=>'North Dakota',
                    'OH'=>'Ohio',
                    'OK'=>'Oklahoma',
                    'OR'=>'Oregon',
                    'PA'=>'Pennsylvania',
                    'RI'=>'Rhode Island',
                    'SC'=>'South Carolina',
                    'SD'=>'South Dakota',
                    'TN'=>'Tennessee',
                    'TX'=>'Texas',
                    'UT'=>'Utah',
                    'VT'=>'Vermont',
                    'VA'=>'Virginia',
                    'WA'=>'Washington',
                    'WV'=>'West Virginia',
                    'WI'=>'Wisconsin',
                    'WY'=>'Wyoming',
                    'ZZ' => 'Other'
                ), null, array('class'=> 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('zip', 'Zip', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('zip', null, array('class'=> 'form-control')) }}
            </div>

            {{ Form::label('country', 'Country', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('country', null, array('class'=> 'form-control')) }}
            </div>
        </div>
    

        <div class="form-group">
            {{ Form::label('gender', 'Gender', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::select('gender', array(
                    'F' => 'Female',
                    'M' => 'Male'
                ), null, array('class'=> 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('shirt_size', 'Shirt Size', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::select('shirt_size', array(
                    'XS' => 'X-Small', 
                    'S' => 'Small', 
                    'M' => 'Medium', 
                    'L' => 'Large',
                    'XL' => 'X-Large'
                ), null, array('class'=> 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('birthdate', 'Birthdate', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('birthdate', null, array('class'=> 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('phone', 'Phone Number', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::text('phone', null, array('class'=> 'form-control')) }}
            </div>
        </div>
    </div>
</div>