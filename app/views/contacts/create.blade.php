@extends('layouts.master')

@section('main')

<h1>Create Contact</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::open(array('route' => 'contacts.store')) }}
    
        
        @include('contacts.form')

        
            {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
        
{{ Form::close() }}

@stop


