@extends('layouts.master')

@section('main')

    <div class="page-header">
      <h1>My Contacts <small>Subheader</small></h1>
    </div>
    <div class="row">
        <div class="pull-left">
            <p>{{ link_to_route('contacts.create', 'Add new ') }}</p>
        </div>
        <div class="pull-right">
            <p>{{ link_to_route('contacts.import', 'Create new contacts from file (.csv)') }}</p>
            <p>{{ link_to_route('contacts.update', 'Update existing contacts using file (.csv)') }}</p>
            <p>{{ link_to_route('contacts.destroy', 'Remove existing contacts using file (.csv)') }}</p>
        </div>
    </div>
    @if ($contacts->count())

        <table class="table table-condensed table-hover table-bordered">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>

            @foreach ($contacts as $contact)
                <tr>
                    <td>{{ link_to_route('contacts.show', $contact->first_name, array($contact->id), array('class' => 'btn')) }}</td>
                    <td>{{{ $contact->last_name }}}</td>
                    <td>{{{ $contact->email }}}</td>
                    
                </tr>
            @endforeach
        </table>

    @else
        There are no contacts. Create them by blah blah blah
    @endif

@stop
