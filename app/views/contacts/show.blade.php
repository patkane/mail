@extends('layouts.master')

@section('main')

    <div class="page-header">
        <h1>{{{ $contact->first_name }}} {{{ $contact->last_name }}}
            </br>
            <small>Manage Your Profile</small>
        </h1>
    </div>

<p>{{ link_to_route('contacts.index', 'Return to all contacts') }}</p>

        <div class="row">
            <div class="col-xs-12 col-md-6 well">

                <div>
                    {{{ $contact->first_name }}} {{{ $contact->last_name }}}
                </div>

                <div>
                    {{{ $contact->email }}}
                </div>

                <div>
                    {{{ $contact->address }}}
                </div>

                <div>
                    {{{ $contact->secondary_address }}}
                </div>

                <div>
                    {{{ $contact->city }}}, {{{ $contact->state }}} {{{ $contact->zip }}}
                </div>

                <div>
                    {{{ $contact->country }}}
                </div>
            

                <div>
                    Gender: {{{ $contact->gender }}}
                </div>

                <div>
                    Shirt Size: {{{ $contact->shirt_size }}}
                </div>

                <div>
                    Birthdate: {{{ $contact->birthdate }}}
                </div>

                <div>
                    Phone: {{{ $contact->phone }}}
                </div>
            </div>

            @foreach ($contact->subscriptions as $subscription)
                {{ Form::button($subscription->name, array('class' => 'btn btn-success')) }}
                    </br>
                @endforeach
                

            
        </div>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
        </tr>
    </thead>

    <tbody>
        <tr>

            <td>{{ link_to_route('contacts.edit', 'Edit', array($contact->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('contacts.destroy', $contact->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
