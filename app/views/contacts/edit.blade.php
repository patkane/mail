@extends('layouts.master')

@section('main')

<h1>Edit {{{ $contact->email }}}</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($contact, array(
    'method' => 'PATCH',
    'class' => 'form-horizontal',
    'role' => 'form',
    'route' => array('contacts.update', $contact->id))) }}
    
        
        @include('contacts.form')

        <div class="col-xs-12 col-md-6 well">
                {{ Form::button(null, array('class' => 'btn btn-success')).' = Subscribe'}}
                {{ Form::button(null, array('class' => 'btn btn-danger')).' = Unsubscribe'}}
                </br>

                @foreach ($subscriptions as $subscription)
                    @if (in_array($subscription->id, $contact->subscriptions))
                        {{ Form::button($subscription->name, array('class' => 'btn btn-success')) }}
                    @else
                        {{ Form::button($subscription->name, array('class' => 'btn btn-danger')) }}
                    @endif

                    </br>
                @endforeach

                </br>
                </br>

                
            </div>

        
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('contacts.show', 'Cancel', $contact->id, array('class' => 'btn')) }}
        

    
{{ Form::close() }}

@stop
