@extends('layouts.scaffold')

@section('main')

<h1>Edit Role</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($role, array('method' => 'PATCH', 'route' => array('roles.update', $role->id))) }}
    <ul>
        
        @include('roles.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('roles.show', 'Cancel', $role->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
