@extends('layouts.master')

@section('main')

<h1>Login</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::open(array('route' => 'sessions.login')) }}
        
        @include('sessions.form')


            {{ Form::submit('Login', array('class' => 'btn btn-info')) }}

{{ Form::close() }}

@stop


