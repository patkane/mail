<div class="row">
    <div class="col-xs-12 col-md-6 well">
        <div class="form-group">
            {{ Form::label('email', 'Email', array('class'=> 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::email('email', null, array('class'=> 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password', 'Password', array('class' => 'col-sm-2 control-label')) }}
            <div class='col-sm-4'>
                {{ Form::password('password', null, array('class'=> 'form-control')) }}
            </div>
        </div>
    </div>
</div>
