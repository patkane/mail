@extends('layouts.master')

@section('headerScripts')
    <script src={{ URL::asset('assets/js/jquery.dataTables.js') }}></script>

    <link href= {{ URL::asset('assets/css/dataTables.bootstrap.css') }} rel="stylesheet">

    <script>
        $(document).ready(function () {
            $('#datatables').dataTable({
                "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
                "sPaginationType": "full_numbers",
                "aoColumns": [
                    { "mData": "name" },
                    { "mData": "subject" },
                    { "mData": "tags" }
                ]
            });
        });
    </script>
@stop

@section('nav')
    @parent
    <!-- <p> appended to master sidebar</p> -->
@stop

@section('main')

<h1><i class="fa fa-envelope fa-3x"></i> Emails</h1>

<p>{{ link_to_route('emails.create', 'Add new ') }}</p>

@if ($emails->count())

    <table id="datatables" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Subject</th>
            <th>Mailing List</th>
            <th>Tags</th>

        </tr>
        </thead>
        <tbody>
        @foreach ($emails as $email)
            <tr>
                    <td><a href="emails/<?php echo $email->id ?>">{{{ $email->name }}}</a></td>
                    <td>{{{ $email->subject }}}</td>
                    <td>list</td>
                    <td>tags</td>
            </tr>
        @endforeach
        </tbody>

    </table>

@else
    There are no emails
@endif

@stop
