@extends('layouts.master')

@section('main')

<h1></h1>
<div class="row">
    <div class="col-md-8">
        <h1>{{{ $email->name }}}</h1>
    </div>
    <div class="col-md-4">
        <div class="col-md-2">
            {{ link_to_route('emails.edit', 'Edit', array($email->id), array('class' => 'btn btn-info')) }}
        </div>
        <div class="col-md-2">
            {{ Form::open(array('method' => 'DELETE', 'route' => array('emails.destroy', $email->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div>
            Name: {{{ $email->name }}}
        </div>
        <div>
            Subject: {{{ $email->subject }}}
        </div>
        <div>
            From Name: {{{ $email->from_name }}}
        </div>
        <div>
            From Email: {{{ $email->from_email }}}
        </div>
        <div>
            Reply To Email: {{{ $email->reply_to_email }}}
        </div>
        <div>
            Status: {{{ $email->status }}}
        </div>
        <div>
            Mailing List: List <!-- link to list-->
        </div>
        <div>
            Send Date: {{{ date('l, F d, Y', strtotime($email->send_date)) }}}
        </div>
        <div>
            Send Time: {{{ date('h:i A', strtotime($email->send_time)) }}}
        </div>
    </div>
    <div class="col-md-8 well">
        {{{ $email->body }}}
    </div>
</div>

<div>
    <p>{{ link_to_route('emails.index', 'Return to all emails') }}</p>
</div>


@stop
