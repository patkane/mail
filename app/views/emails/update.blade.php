@extends('layouts.scaffold')

@section('main')

<h1>Edit Email</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($email, array('method' => 'PATCH', 'route' => array('emails.update', $email->id))) }}
    <ul>
        
        @include('emails.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('emails.show', 'Cancel', $email->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
