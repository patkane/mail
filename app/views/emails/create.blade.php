@extends('layouts.master')

@section('headerScripts')

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    <!-- include summernote css/js-->
    <link href="{{ URL::asset('assets/css/summernote.css') }}" rel="stylesheet"/>
    <script type="text/javascript" src="{{ URL::asset('assets/js/summernote.min.js') }}"></script>

@stop

@section('nav')

    @parent

    <!-- <p> appended to master sidebar</p> -->

@stop

@section('main')

    <h1>Create Email</h1>


    {{ Form::open(array('route' => 'emails.store')) }}
        
            
        @include('emails.form')

            
        {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
            
        
    {{ Form::close() }}

@stop


@section('bodyScripts')

    <link href="{{ URL::asset('assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
    <script type="text/javascript" src="{{ URL::asset('assets/js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                pickTime: false
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker2').datetimepicker({
                pickDate: false
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });
    </script>
      
@stop