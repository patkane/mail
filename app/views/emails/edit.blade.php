@extends('layouts.master')

@section('headerScripts')

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    <!-- include summernote css/js-->
    <link href="{{ URL::asset('assets/css/summernote.css') }}" rel="stylesheet"/>
    <script type="text/javascript" src="{{ URL::asset('assets/js/summernote.min.js') }}"></script>

@stop

@section('nav')

    @parent

    <!-- <p> appended to master sidebar</p> -->

@stop

@section('main')

    <ol class="breadcrumb">
        <li><a href="{{ URL::to('emails') }}">Emails</a></li>
        <li class="active">{{{ $email->name }}}</a></li>
    </ol>

    <div class='container'>
        <div class="page-header col-md-12">
            <h1>{{{ $email->name }}}</h1>

            {{ Form::model($email, array(
                'method' => 'PATCH',
                'id' => 'form1',
                'route' => array('emails.update', $email->id),
                'class' => 'form-horizontal',
                'role' => 'form'
            )) }}

            <div class="form-group">
                <div class="form-group col-md-6 pull-right">
                    {{ Form::label('status', 'Status', array('class' => 'col-md-2 control-label')) }}
                    <div class='col-md-4'>
                        {{ Form::select('status', array(
                            'Draft' => 'Draft',
                            'Ready for Review' => 'Ready for Review',
                            'Active' => 'Active'
                        ), null, array('class'=> 'form-control')) }}
                    </div>
                </div>
            </div>
        </div>

        @include('emails.form')

        {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
        {{ link_to_route('emails.show', 'Cancel', $email->id, array('class' => 'btn btn-default')) }}
        
        {{ Form::close() }}

    </div>

@stop

@section('bodyScripts')

    <link href="{{ URL::asset('assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
    <script type="text/javascript" src="{{ URL::asset('assets/js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                pickTime: false
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker2').datetimepicker({
                pickDate: false
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });
    </script>
      
@stop
