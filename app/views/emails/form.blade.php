<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            {{ Form::label('name', 'Email Name', array('class'=> 'col-sm-4 control-label')) }}
            <div class='col-sm-8'>
                {{ Form::text('name', null, array('class'=> 'form-control')) }}
                @if ($errors->all())
                    {{ $errors->first('name') }}
                @endif
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('subject', 'Email Subject', array('class' => 'col-sm-4 control-label')) }}
            <div class='col-sm-8'>
                {{ Form::text('subject', null, array('class'=> 'form-control')) }}
                @if ($errors->all())
                    {{ $errors->first('subject') }}
                @endif
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('from_name', 'From Name', array('class' => 'col-sm-4 control-label')) }}
            <div class='col-sm-6'>
                {{ Form::text('from_name', null, array('class'=> 'form-control')) }}
                @if ($errors->all())
                    {{ $errors->first('from_name') }}
                @endif
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('from_email', 'From Email', array('class' => 'col-sm-4 control-label')) }}
            <div class='col-sm-6'>
                {{ Form::text('from_email', null, array('class'=> 'form-control')) }}
                @if ($errors->all())
                    {{ $errors->first('from_email') }}
                @endif
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('reply_to_email', 'Reply To Email', array('class' => 'col-sm-4 control-label')) }}
            <div class='col-sm-6'>
                {{ Form::text('reply_to_email', null, array('class'=> 'form-control')) }}
                @if ($errors->all())
                    {{ $errors->first('reply_to_email') }}
                @endif
            </div>
        </div>

        @if(isset($email))

            <div class="form-group">
                {{ Form::label('send_date', 'Send Date', array('class' => 'col-sm-4 control-label')) }}
                <div class='col-sm-6'>
                    {{ Form::text('send_date', date('m/d/Y', strtotime($email->send_date)), array(
                        'id'=>'datetimepicker1',
                        'class'=> 'date form-control',
                        'data-format' => 'MM/DD/YYYY'
                    )) }}
                    @if ($errors->all())
                    {{ $errors->first('send_date') }}
                @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('send_time', 'Send Time', array('class' => 'col-sm-4 control-label')) }}
                <div class='col-sm-6'>
                    {{ Form::text('send_time', date('h:i A', strtotime($email->send_time)), array(
                        'id'=>'datetimepicker2',
                        'class'=> 'date form-control'
                    )) }}
                    @if ($errors->all())
                    {{ $errors->first('send_time') }}
                @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('audience', 'Audience', array('class' => 'col-sm-4 control-label')) }}
                <div class='col-sm-6'>
                    {{ Form::select('audience', array(
                        'mailing_list' => 'Mailing List',
                        'Subscription' => 'Subscription',
                    ), null, array('class'=> 'form-control')) }}
                </div>
            </div>

        @else
            <div class="form-group">
                {{ Form::label('send_date', 'Send Date', array('class' => 'col-sm-4 control-label')) }}
                <div class='col-sm-6'>
                    {{ Form::text('send_date', null, array(
                        'id'=>'datetimepicker1',
                        'class'=> 'date form-control',
                        'data-format' => 'MM/DD/YYYY'
                    )) }}
                    @if ($errors->all())
                    {{ $errors->first('send_date') }}
                @endif
                </div>
            </div>
        

            <div class="form-group">
                {{ Form::label('send_time', 'Send Time', array('class' => 'col-sm-4 control-label')) }}
                <div class='col-sm-6'>
                    {{ Form::text('send_time', null, array(
                        'id'=>'datetimepicker2',
                        'class'=> 'date form-control'
                    )) }}
                    @if ($errors->all())
                    {{ $errors->first('send_time') }}
                @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('audience', 'Audience', array('class' => 'col-sm-4 control-label')) }}
                <div class='col-sm-6'>
                    {{ Form::select('audience', array(
                        'mailing_list' => 'Mailing List',
                        'Subscription' => 'Subscription',
                    ), null, array('class'=> 'form-control')) }}
                </div>
            </div>

        @endif
    </div>


</div>

</br>









</br>

@if(isset($email))

    <textarea class="input-block-level" id="summernote" name="body">{{{ $email->body }}}</textarea>
@else

    <textarea class="input-block-level" id="summernote" name="body"></textarea>

@endif


        