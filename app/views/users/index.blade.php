@extends('layouts.master')

@section('content')

<h1>All Users</h1>

<p>{{ link_to_route('users.create', 'Add new ') }}</p>

@if ($users->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>first_name</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->first_name }}</td>

                    <td>{{ link_to_route('users.show', 'View', array($user->id), array('class' => 'btn')) }}</td>

                    <td>{{ link_to_route('users.edit', 'Edit', array($user->id), array('class' => 'btn btn-info')) }}</td>

                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('users.destroy', $user->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no users
@endif

@stop
