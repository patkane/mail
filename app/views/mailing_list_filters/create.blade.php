@extends('layouts.master')

@section('main')

<h1>Create Mailing List</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::open(array('route' => 'mailing_lists.store')) }}
    <ul>
        
        @include('mailing_lists.form')
        @include('mailing_list_filters.form')

        <li>
            {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
        </li>
    </ul>
{{ Form::close() }}

@stop


