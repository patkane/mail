<div class="row">
    <div class="form-group">
        {{ Form::label('operator_column', 'Select contacts whose', array('class'=> 'col-sm-2 control-label')) }}
        <div class='col-sm-2'>
            {{ Form::select('operator_column', array(
                'default' => '',
                'age' => 'age', 
                'gender' => 'gender'
            ), null, array('class'=> 'form-control')) }}
        </div>

        {{ Form::label('operator', ' ', array('class' => 'col-sm-0 control-label')) }}
        <div class='col-sm-3'>
            {{ Form::select('operator', array(
                'default' => '',
                '=' => 'is equal to',
                '>' => 'is greater than',
                '>=' => 'is greater than or equal to',
                '<' => 'is less than',
                '<=' => 'is less than or equal to',
                'BETWEEN' => 'is between'
            ), null, array('class'=> 'form-control')) }}
        </div>

        
        <?php $age_list = array('default' => '');
        foreach (range(1, 100) as $number) {
            $age_list[$number] = $number;
        }
        ?>

        {{ Form::label('column_value', ' ', array('class' => 'col-sm-0 control-label')) }}
        <div class='col-sm-2'>
            {{ Form::select('column_value', $age_list, null, array('class'=> 'form-control')) }}
        </div>
    </div>
</div>
