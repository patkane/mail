@extends('layouts.master')

@section('main')

<h1>All MailingLists</h1>

<p>{{ link_to_route('mailing_lists.create', 'Add new ') }}</p>

@if ($mailing_lists->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>id</th>
                <th>user_id</th>
                <th>operator_name</th>
                <th>operator</th>
                <th>operator_value</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($mailing_lists as $mailing_list)
                <tr>
                    <td>{{ $mailing_list->id }}</td>
                    <td>{{ $mailing_list->user_id }}</td>
                    <td>{{ $mailing_list->operator_name }}</td>
                    <td>{{ $mailing_list->operator }}</td>
                    <td>{{ $mailing_list->operator_value }}</td>

                    <td>{{ link_to_route('mailing_lists.show', 'View', array($mailing_list->id), array('class' => 'btn')) }}</td>

                    <td>{{ link_to_route('mailing_lists.edit', 'Edit', array($mailing_list->id), array('class' => 'btn btn-info')) }}</td>

                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('mailing_lists.destroy', $mailing_list->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no mailing_lists
@endif

@stop
