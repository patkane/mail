@extends('layouts.scaffold')

@section('main')

<h1>Edit Admin</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($admin, array('method' => 'PATCH', 'route' => array('admins.update', $admin->id))) }}
    <ul>
        
        @include('admins.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('admins.show', 'Cancel', $admin->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
