@extends('layouts.scaffold')

@section('main')

<h1>Create Admin</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::open(array('route' => 'admins.store')) }}
    <ul>
        
        @include('admins.form')

        <li>
            {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
        </li>
    </ul>
{{ Form::close() }}

@stop


