@extends('layouts.master')


@section('nav')

    @parent
    <!-- <p> appended to master sidebar</p> -->

@stop


@section('main')

<style>
    @import url(https://fonts.googleapis.com/css?family=Lato:300,400,700);
    body {
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        font-family: 'Lato';
    }
    button {
        border-radius: 0 0 4px 4px !important;
        
    }
    .package {
        background-color: #B2CCFF;
        
        border-radius: 4px;
        text-align: center;
        padding: 10px;
        margin: 0 0 20px;
        height: 155px;
        position: relative;
    }
    .package .title,
    .package .count {
        display: table;
        height: 50%;
        width: 100%;
    }
    h1 {
        display: table-cell;
        vertical-align: middle;
        font-family: 'Lato';
        font-weight: 300;
        font-size:24px;
        color: #333;
        margin-bottom: 10px;
    }
    h2 {
        display: table-cell;
        vertical-align: top;
        font-family: 'Lato';
        font-weight: 400;
        font-size: 60px;
        color: #666;
        margin: 0 0;
        line-height: 55px;
    }
    .row {margin:0 0;position: relative;}
    .row > .row {
        margin-left: -15px;
        margin-right:-15px;
    }
</style>



    <div class="page-header">
      <h1>Admin
      </br><small>Manage</small></h1>
    </div>
    

    
    <div class="row" style="padding:0px 50px;margin-top:50px;">
        <div class="row">
            <div class="col-md-3 ">
                <a href="{{ URL::to('users') }}">
                    <div class="package">
                        <div class="title"><h1>Users</h1></div>
                        <div class="count"><h2><i class="fa fa-user"></i></h2></div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 ">
                <a href="{{ URL::to('teams') }}">
                    <div class="package">
                        <div class="title"><h1>Teams</h1></div>
                        <div class="count"><h2><i class="fa fa-users"></i></h2></div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 ">
                <a href="{{ URL::to('roles') }}">
                    <div class="package">
                        <div class="title"><h1>Roles</h1></div>
                        <div class="count"><h2><i class="fa fa-sitemap"></i></h2></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>


@stop
