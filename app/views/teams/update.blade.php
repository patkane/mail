@extends('layouts.scaffold')

@section('main')

<h1>Edit Team</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($team, array('method' => 'PATCH', 'route' => array('teams.update', $team->id))) }}
    <ul>
        
        @include('teams.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('teams.show', 'Cancel', $team->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
