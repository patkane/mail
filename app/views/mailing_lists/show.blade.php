@extends('layouts.master')

@section('main')

    <h1>{{ $mailing_list->name }}</h1>

    <p>{{ link_to_route('mailing_lists.index', 'Return to all mailing lists') }}</p>

    <div class="row pull-right">
        {{ link_to_route('mailing_lists.edit', 'Edit', array($mailing_list->id), array('class' => 'btn btn-info')) }}

        {{ Form::open(array('method' => 'DELETE', 'route' => array('mailing_lists.destroy', $mailing_list->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}

    </div>

    </br>
    </br>
    </br> 
    <div class="row">
        <div>
            Name: {{ $mailing_list->name }}
        </div>
        <div>
            List Size: list size
        </div>
        <div>
            Filters: filter list
        </div>
        <div>
            Tags: tags
        </div>
        <div>
            Upcoming Emails Using This List: upcoming email list
        </div>
        <div>
            Emails that have used this List: past email list
        </div>
        <div>
            Created By: created by
        </div>
        <div>
            Created On: created On
        </div>
    </div>   
      

@stop
