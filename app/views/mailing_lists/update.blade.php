@extends('layouts.scaffold')

@section('main')

<h1>Edit MailingList</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($mailing_list, array('method' => 'PATCH', 'route' => array('mailing_lists.update', $mailing_list->id))) }}
    <ul>
        
        @include('mailing_lists.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('mailing_lists.show', 'Cancel', $mailing_list->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
