@extends('layouts.master')

@section('main')

<h1>My Mailing Lists</h1>

<p>{{ link_to_route('mailing_lists.create', 'Add new ') }}</p>

@if ($mailing_lists->count())
    <table class="table table-condensed table-hover table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th>Audience Size</th>
                <th>Base List</th>
                <th>Filters</th>
                <th>Tags???</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($mailing_lists as $mailing_list)
                <tr>

                    <td>{{ link_to_route('mailing_lists.show', $mailing_list->name, array($mailing_list->id), array('class' => 'btn')) }}</td>

                    <td>list size</td>
                    <td>my contacts, subscription name, or custom</td>
                    <td>yes or no, count</td>
                    <td>tag list</td>

                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no mailing lists. Create one now?
@endif

@stop
