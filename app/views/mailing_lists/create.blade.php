@extends('layouts.master')

@section('main')

<h1>Create Mailing List</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::open(array('route' => 'mailing_lists.store')) }}
    
        
    @include('mailing_lists.form')

    </br>
    </br>
    </br>

    Add Filter Criteria:
    @include('mailing_list_filters.form')

    
    {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
     
{{ Form::close() }}

@stop


