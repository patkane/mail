<div class="row">
    
    <div class="form-group">
        {{ Form::label('name', 'Name', array('class'=> 'col-sm-2 control-label')) }}
        <div class='col-sm-10'>
            {{ Form::text('name', null, array('class'=> 'form-control')) }}
        </div>
    </div>


    <div class="form-group">
        {{ Form::label('base_list', 'Create List from:', array('class' => 'col-sm-2 control-label')) }}
        <div class='col-sm-4'>
            {{ Form::select('base_list', array(
                'contacts' => 'My Contact List',
                'subscription' => 'An Active Subscription',
                'import' => 'A Custom List' 
            ), null, array('class'=> 'form-control')) }}
        </div>
    </div>
</div>

