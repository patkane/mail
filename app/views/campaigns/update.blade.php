@extends('layouts.scaffold')

@section('main')

<h1>Edit Campaign</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::model($campaign, array('method' => 'PATCH', 'route' => array('campaigns.update', $campaign->id))) }}
    <ul>
        
        @include('campaigns.form')

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('campaigns.show', 'Cancel', $campaign->id, array('class' => 'btn')) }}
        </li>

    </ul>
{{ Form::close() }}

@stop
