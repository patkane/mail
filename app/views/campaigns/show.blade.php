@extends('layouts.master')

@section('main')

<h1>Show Campaign</h1>

<p>{{ link_to_route('campaigns.index', 'Return to all campaigns') }}</p>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
        </tr>
    </thead>

    <tbody>
        <tr>

            <td>{{ link_to_route('campaigns.edit', 'Edit', array($campaign->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('campaigns.destroy', $campaign->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
