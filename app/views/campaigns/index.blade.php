@extends('layouts.master')

@section('headerScripts')
    

    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/calendar.css') }}">
    
    
@stop

@section('nav')
    @parent
    <!-- <p> appended to master sidebar</p> -->
@stop

@section('main')

<h1>All Campaigns</h1>

<p>{{ link_to_route('campaigns.create', 'Add new ') }}</p>

    <div class='container'>

        <div class="page-header">
            

            <div class="pull-right form-inline">
                <div class="btn-group">
                    <button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
                    <button class="btn btn-default" data-calendar-nav="today">Today</button>
                    <button class="btn btn-primary" data-calendar-nav="next">Next >></button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-warning" data-calendar-view="year">Year</button>
                    <button class="btn btn-warning active" data-calendar-view="month">Month</button>
                    <button class="btn btn-warning" data-calendar-view="week">Week</button>
                    <button class="btn btn-warning" data-calendar-view="day">Day</button>
                </div>
            </div>

            <h3></h3>  <!-- Month Heading-->
        </div>

    <div class="row">
        <div class="col-md-12">
            <div id="calendar"></div>
        </div>
        <!-- <div class="col-md-3">

            <h4>Events</h4>
            <small>List of upcoming campaign events</small>
            <ul id="eventlist" class="nav nav-list"></ul>
        </div> -->
    </div>

    <div class="clearfix"></div>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/underscore-min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/calendar.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/app.js') }}"></script>
    
    <script type="text/javascript">
        var calendar = $('#calendar').calendar({events_source: '{{ URL::asset("assets/events.json.php") }}'});
    </script>
    
</div>

@if ($campaigns->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
            </tr>
        </thead>

        <tbody>
            @foreach ($campaigns as $campaign)
                <tr>

                    <td>{{ link_to_route('campaigns.show', 'View', array($campaign->id), array('class' => 'btn')) }}</td>

                    <td>{{ link_to_route('campaigns.edit', 'Edit', array($campaign->id), array('class' => 'btn btn-info')) }}</td>

                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('campaigns.destroy', $campaign->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no upcoming campaign activities
@endif

@stop
